# FourSquare Venues Search - ANDigital Coding Exercise

You can open the index.html in browser and search for a location to see the result. You can either hit enter after you entered the city name or click the search buttons

# Before code changes

- npm install
- npm install grunt-cli

# Grunt tasks available

- **grunt watch**: watches for less changes/JavaScript changes and GruntFile config changes
- **grunt less**: less to css & minification (css/style.css)
- **grunt uglify**: javascript minification (dist/script.js)

# Approach

In order to not overengineer things, i used jQuery and did just a bit of JavaScript. The approach is simple, everytime you hit enter after typing in the input, 
it triggers a change event on that input and an ajax call is made to retrieve the venues around that location. Same applies for the search button.
