module.exports = function(grunt) {
  grunt.initConfig({
    // Less to css task
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "css/style.css": "less/style.less"
        }
      }
    },

    // Watch tasks
    watch: {
      // Styles
      css: {
        files: ['less/**/*.less'],
        tasks: ['less'],
        options: {
          nospawn: true
        }
      },

      // JavaScript
      js: {
        files: ['js/**/*.js'],
        tasks: ['uglify'],
        options: {
          spawn: false,
        },
      },

      // Gruntconfig
      configFiles: {
        files: ['Gruntfile.js'],
        options: {
          reload: true
        }
      }
    },

    uglify: {
      my_target: {
        files: {
          'dist/script.js': ['js/app.js']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['watch', 'less', 'uglify', 'requirejs']);
};