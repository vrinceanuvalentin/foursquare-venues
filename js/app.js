$(document).ready(function() {
    var searchInput = $('.search-input'),
        searchButton = $('.search-button'),
        locationContainer = $('.venues ul'),
        clientId = 'UR1TQ5VKLS4TEDJCIKHZCJULHOIIEIMEIC1FM5MRWUFARTHV',
        clientSecret = 'WYPSQWT5SNKJWSCXGI1PRCM4UBTUBAJ3OTJDMX2VEVWS0I4O',
        jsonData,
        venues;

    searchInput.val('');

    searchInput.on('change', function() {
        initialiseSearch();
    })

    searchButton.on('click', searchButton, function(e) {
        e.preventDefault();
        initialiseSearch();
    });

    function initialiseSearch() {
        if (searchInput.val().length < 1) {
            return;
        }

        locationContainer.html('');
        jsonData = 'https://api.foursquare.com/v2/venues/search?near=' + searchInput.val() + '&client_id=' + clientId + '&client_secret=' + clientSecret + '&v=20130815',
        retrieveVenues(jsonData);
    }

    function retrieveVenues(jsonData) {
        $.ajax({
            dataType: 'json',
            cache: false,
            type: 'get',
            url: jsonData,
            success: manipulateData
        });
    }

    function manipulateData(data) {
        for(var i = 0; i < data.response.venues.length; i++) {
            var location,
                name = data.response.venues[i].name,
                checkins = data.response.venues[i].stats.checkinsCount;

            if (data.response.venues[i].location.address === undefined) {
                var location = data.response.venues[i].location.country;
            } else {
                location = data.response.venues[i].location.address;
            }

            locationContainer.append('<li> <span>' + name + '</span> <span> Checkins: ' + checkins + '</span> <span> Location: ' + location + '</span></li>');
        }
    }
});
    